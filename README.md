# Challenge #1. Number conversion


### Description

Write a program that would take a number and return its English representation. Example:

- *Input*: `42`
- *Output*: `'forty two'`

For implementation use the `convertNumberToEnglishText(n)` function located in `converter.py`.
 

### Project setup

You need to have `python3` installed on your machine.


### Requirements checklist

1. All test cases that are currently in `test_converter.py` must pass. You can run them with `python3 -m unittest test_converter.py`.
2. Skip the word "and" for larger numbers, i.e. `two thousand one` instead of `two thousand and one`.
3. The implementation must cover cases from `-99999` to `99999`.
